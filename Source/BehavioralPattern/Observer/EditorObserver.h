// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "EditorObserver.generated.h"

/** @brief Pointer to a void function without argument.*/
template<class TClass>
class TFuncPtr final 
{
private:
	typedef void (TClass::*FuncPtr)();

public:
	explicit TFuncPtr(TClass* Obj, FuncPtr Func)
	: Object(Obj)
	,Function(Func)
	{
		
	}

	bool operator==(const TFuncPtr& V) const
	{
		return Object == V.Object && Function == V.Function;
	};

	void Execute()
	{
		(Object->*Function)();
	}

private:
	TClass* Object{ nullptr };
	FuncPtr Function;
};

/**
 * @brief An observer class that will exist only when the editor is open.
 * @note Can be found with TObjectInterator
 */
UCLASS()
class BEHAVIORALPATTERN_API UEditorObserver : public UObject
{
	GENERATED_BODY()

	using Function = TFuncPtr<UObject>;

public:
	/** @brief Add a function to a subscribers list. */
	void Subscribe(Function FuncPtr);

	/** @brief Remove a function from a subscribers list. */
	void Unsubscribers(Function FuncPtr);

	/** @brief Notify subscribers. */
	void Broadcast();
	
private:
	/** @brief A list of all functions waiting for something to happen. */
	TArray<Function> Subscribers;
};

inline void UEditorObserver::Subscribe(Function FuncPtr)
{
	Subscribers.Emplace(FuncPtr);
}

inline void UEditorObserver::Unsubscribers(Function FuncPtr)
{
	Subscribers.RemoveSingle(FuncPtr);
}

inline void UEditorObserver::Broadcast()
{
	for(auto& FunctPtr : Subscribers)
	{
		FunctPtr.Execute();
	}
}
