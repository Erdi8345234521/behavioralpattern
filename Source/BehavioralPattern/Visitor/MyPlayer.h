// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SavableActor.h"
#include "GameFramework/Character.h"
#include "MyPlayer.generated.h"

UCLASS()
class BEHAVIORALPATTERN_API AMyPlayer : public ACharacter, public ISavableActor
{
	GENERATED_BODY()

public:
	FName GetPlayerName() const { return PlayerName; }
	float GetCurrentHealth() const { return CurrentHealth; }

	virtual void SaveData_Implementation(UGameSaver* Saver);

private:
	FName PlayerName;
	float CurrentHealth;

};