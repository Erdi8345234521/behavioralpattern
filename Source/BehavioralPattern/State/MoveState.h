// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"
#include "MoveState.generated.h"


UENUM(BlueprintType, Blueprintable)
enum class EMoveState : uint8
{
	EMS_Idle = 0,
	EMS_Walk = 1,
	EMS_Run = 2,
	EMS_ExhaustBegin = 3,
	EMS_ExhaustEnd = 4,
	EMS_Fall = 5
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FStateChanged, EMoveState, NewState);

/** @brief A base state for other objects to work with. **/
USTRUCT(BlueprintType, Blueprintable)
struct FMoveState
{
	GENERATED_BODY()

public:
	FStateChanged OnStateChanged;

	bool operator==(const FMoveState& V) const
	{
		return StateType == V.StateType &&
				Owner == V.Owner &&
				SpeedX == V.SpeedX &&
				SpeedY == V.SpeedY &&
				SpeedZ == V.SpeedZ;
				
	};

public:
	/** @brief We override ctor to imlement data load from some data table. */
	explicit FMoveState(EMoveState Type, APlayerController* Controller)
		: StateType(Type)
		, Owner(Controller)
	{
		LoadStateInfo();
	}

	FMoveState() = default;
	virtual ~FMoveState() = default;

	/** @return Enum type bound to this state */
	EMoveState GetStateType() const { return StateType; }

	/** @brief Any method that shall be called ance this event is set must be here. */
	virtual void ApplyState(){}

	/** @brief MoveForward */
	virtual void MoveAlongX(float Value)
	{
		if(GetOwner())
		{
			Value += SpeedX * GetOwner()->GetWorld()->GetDeltaSeconds();
			const FVector Direction = FRotationMatrix(GetControllerYaw()).GetUnitAxis(EAxis::X);

			GetOwner()->GetCharacter()->AddMovementInput(Direction, Value);
		}
	}

	/** Move sideways. */
	virtual void MoveAlongY(float Value)
	{
		if(GetOwner())
		{
			Value += SpeedY * GetOwner()->GetWorld()->GetDeltaSeconds();
			const FVector Direction = FRotationMatrix(GetControllerYaw()).GetUnitAxis(EAxis::Y);

			GetOwner()->GetCharacter()->AddMovementInput(Direction, Value);
		}
	}

	/** Simple rotation. */
	virtual void RotateAroundZ(float Angle)
	{
		if(GetOwner())
		{
			Angle += SpeedZ * GetOwner()->GetWorld()->GetDeltaSeconds();
			GetOwner()->AddYawInput(Angle);
		}
	}

	/** @brief By default sets state to idle. */
	virtual void Stop()
	{
		OnStateChanged.Broadcast(EMoveState::EMS_Idle);
	}

protected:
	/** @return Owning player controller. */
	APlayerController* GetOwner() const
	{
		return Owner;
	}

	/** @return Current controller's yaw rotation. */
	FRotator GetControllerYaw() const
	{
		FRotator Rot;
		if(Owner)
		{
			Rot = FRotator(0.f, Owner->GetControlRotation().Yaw, 0.f);
		}
		return Rot;
	}

	/** @brief Assume we load info from same data-table or else. */
	virtual void LoadStateInfo(){};

private:
	/** @brief Enum representation of the state. */
	EMoveState StateType;

	/** @brief Character owning player controller. */
	UPROPERTY()
	APlayerController* Owner;

	/** @brief Forward movement speed. */
	float SpeedX;

	/** @brief Sideways movement speed. */
	float SpeedY;

	/** @brief Rotation speed. */
	float SpeedZ;
};

USTRUCT()
struct FIdleState : public FMoveState
{
	GENERATED_BODY()
public:
	virtual void MoveAlongX(float Value) override
	{
		OnStateChanged.Broadcast(EMoveState::EMS_Walk);
		Super::MoveAlongX(Value);
	}

	virtual void MoveAlongY(float Value) override
	{
		OnStateChanged.Broadcast(EMoveState::EMS_Walk);
		Super::MoveAlongY(Value);
	}

	/** @brief While idle we do nothing to stop. */
	virtual void Stop() override {};
};

USTRUCT()
struct FWalkState : public FMoveState
{
	GENERATED_BODY()
};

USTRUCT()
struct FRunState : public FMoveState
{
	GENERATED_BODY()
public:
	virtual void MoveAlongX(float Value) override
	{
		Super::MoveAlongX(Value);
		UpdateStamina();
	}

protected:
	/** @brief Override it to get additional fields. */
	virtual void LoadStateInfo() override {};

	/** @brief We want to update our stamina each frame. */
	void UpdateStamina()
	{
		Stamina -= GetOwner()->GetWorld()->GetDeltaSeconds();
		if(Stamina <= 0.f)
		{
			OnStateChanged.Broadcast(EMoveState::EMS_ExhaustBegin);
			Stamina = StaminaMax;
		}
	}

private:
	/** @brief Full stamina value. */
	float StaminaMax = 0.f;

	/** @brief Current stamina value. */
	float Stamina = 0.f;
};

USTRUCT()
struct FExhaustedState : public FMoveState
{
	GENERATED_BODY()
public:
	virtual void ApplyState() override
	{
		StartCooldown();
	}
	
	/** @brief Once in this state we shall start counting down cooldown time. */
	void StartCooldown() {};

protected:
	/** @brief Override it to get additional fields. */
	virtual void LoadStateInfo() override {};

	/** @brief Once cooled down call this. */
	void OnCooldown_Callback()
	{
		OnStateChanged.Broadcast(EMoveState::EMS_ExhaustEnd);
	}

private:
	/** @brief Cooldown duration */
	float CooldownTime = 0.f;
};

USTRUCT()
struct FFallState : public FMoveState
{
	GENERATED_BODY()
public:
	/** This state wan't call any parent's
	 *  functions, couse no input shall be applied
	 */
	virtual void MoveAlongX(float Value) override {}
	virtual void MoveAlongY(float Value) override {}
	virtual void RotateAroundZ(float Angle) override {}
};