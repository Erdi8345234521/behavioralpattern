// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Collection/ConcreteCollectionObject.h"
#include "Collection/TestCollectionObject.h"
#include "GameFramework/Actor.h"
#include "Iterators/TestIterator.h"
#include "Kismet/KismetSystemLibrary.h"



#include "ClientCode.generated.h"

UCLASS()
class BEHAVIORALPATTERN_API AClientCode : public AActor
{
	GENERATED_BODY()

protected:
	UConcreteCollectionObject* ConcreteCollectionObject;
	UTestCollectionObject* TestCollectionObject;

public:	
	void Config();

	void UseIterator();
};

inline void AClientCode::Config()
{
	ConcreteCollectionObject = NewObject<UConcreteCollectionObject>();
	TestCollectionObject = NewObject<UTestCollectionObject>();
}

inline void AClientCode::UseIterator()
{
	/*auto ConcreteObject = ConcreteCollectionObject->CreateFriendsIterator();
	auto TestObject = TestCollectionObject->CreateFriendsIterator();
	
	while(TestObject->HasMore())
	{
		auto TestIterator = Cast<UTestIterator>(TestObject->GetNext());
		UKismetSystemLibrary::PrintString(this, TestIterator ? "Null" : TestIterator->GetName()); 
	}*/
	
	//Use Iterator
}
