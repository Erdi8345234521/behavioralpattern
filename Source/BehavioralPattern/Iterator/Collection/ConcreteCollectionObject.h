// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CollectionObject.h"
#include "ConcreteCollectionObject.generated.h"

/**
 * 
 */
UCLASS()
class BEHAVIORALPATTERN_API UConcreteCollectionObject : public UCollectionObject
{
	GENERATED_BODY()

public:
	virtual UIterator* CreateFriendsIterator() override;
};


