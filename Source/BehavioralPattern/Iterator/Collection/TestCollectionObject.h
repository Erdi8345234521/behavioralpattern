// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CollectionObject.h"
#include "TestCollectionObject.generated.h"

/**
 * 
 */
UCLASS()
class BEHAVIORALPATTERN_API UTestCollectionObject : public UCollectionObject
{
	GENERATED_BODY()

public:
	virtual UIterator* CreateFriendsIterator() override;
};
