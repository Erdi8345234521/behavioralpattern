// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Iterator.h"
#include "BehavioralPattern/Iterator/Collection/TestCollectionObject.h"
#include "UObject/NoExportTypes.h"
#include "TestIterator.generated.h"

/**
 * 
 */
UCLASS()
class BEHAVIORALPATTERN_API UTestIterator : public UIterator
{
	GENERATED_BODY()

public:
	void SetCollection(UTestCollectionObject* CollectionObject)
	{
		ConcreteCollection = CollectionObject;
	};

	void AddObject(UTestCollectionObject* Object)
	{
		Cache.Add(Object);
	}
	
	virtual UCollectionObject* GetNext() override;

	virtual bool HasMore() override;
	
private:
	UPROPERTY()
	class UTestCollectionObject* ConcreteCollection;

	int32 CurrentPosition;

	UPROPERTY()
	TArray<UTestCollectionObject*> Cache;
};

inline UCollectionObject* UTestIterator::GetNext()
{
	if(HasMore())
	{
		auto Object = Cast<UTestCollectionObject>(Cache[CurrentPosition]);
		CurrentPosition++;
		return Object;
	}

	return nullptr;
}

inline bool UTestIterator::HasMore()
{
	return CurrentPosition < Cache.Num();
}
