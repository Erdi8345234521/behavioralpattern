// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BehavioralPatternGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BEHAVIORALPATTERN_API ABehavioralPatternGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
