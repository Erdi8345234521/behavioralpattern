// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Step.h"
#include "Round.generated.h"

class ACharacter;

/**
 * @brief Some simple round representation.
 * @note We also may want to use Command-pattern
 * in case of network-based games to stack action
 * while waiting for server to correct us.
 */
UCLASS(BlueprintType, Blueprintable)
class BEHAVIORALPATTERN_API URound : public UObject
{
	GENERATED_BODY()

public:
	/** @brief Starting the round for same character. */
	void StartRoundOf(ACharacter* RoundOwner);

	/** @brief Add a step to make once running. */
	void AddStep(FStep* Step);

	/** @brief Removing a step from our stack. */
	void UndoStep();

	/** @brief Running a round for same character and executing all the steps placed into stack. */
	void RunRound();

private:
	/** @brief Current round's owner. */
	UPROPERTY()
	ACharacter* Owner;

	/** @brief Stack of steps to be taken this round. */
	TArray<FStep> Steps;
};

inline void URound::StartRoundOf(ACharacter* RoundOwner)
{
	Owner = RoundOwner;
	Steps.Empty();
}

inline void URound::AddStep(FStep* Step)
{
	if(Step)
	{
		Steps.Emplace(std::move(*Step));
	}
}

inline void URound::UndoStep()
{
	Steps.Pop();
}

inline void URound::RunRound()
{
	for(auto& Step : Steps)
	{
		Step.Execute(Owner);
	}
}



